const express = require('express')
const app = express()
const http = require('http').createServer(app)
const io = require('socket.io')(http)

const PORT = 5000 || process.env.PORT

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

io.on('connection', socket => {
  socket.on('chat message', msg => {
    console.log('message: ' + msg)
  })
})

http.listen(PORT, () => {
  console.log(`Running on ${PORT}`)
})
//omae
//nani
